#include "PropertyBrowserWidget.h"
#include <QApplication>
#include <QDebug>
#include <QLayout>
#include <QFile>
#include <QJsonDocument>

struct Baka
{

    double d;
    int i;
    bool b;

    Baka(){
        d=-0.3;
        i=-7;
        b=false;
    }

    void setD(double v)
    {
        d=v;
        qDebug()<<"setD:"<<d<<endl;
    }

    double getD()
    {
        return d;
    }

    void setI(int v)
    {
        i=v;
        qDebug()<<"setI:"<<i<<endl;
    }

    int getI()
    {
        qDebug()<<"getI:"<<i<<endl;
        return i;
    }

    void setB(bool v)
    {
        b=v;
        qDebug()<<"setB:"<<b<<endl;
    }

    bool getB()
    {
        return b;
    }
} baka;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PropertyBrowserWidget w;
    w.show();

    Property<int> p1("I",&Baka::getI, &Baka::setI,baka);
    Property<double> p2("D",&Baka::getD, &Baka::setD,baka);

    w.setTitle("Baka");
    w.addProperty(p1);
    w.addProperty(p2);

    p1.set(8);


    QFile file(QStringLiteral("save.json"));
    if (!file.open(QIODevice::ReadOnly/*WriteOnly*/)) {
        qWarning("Couldn't open save file.");
        return false;
    }
//    QJsonObject gameObject;
//    w.write(gameObject);
//    QJsonDocument doc(gameObject);
//    file.write(doc.toJson());
//    file.close();

    QByteArray saveData = file.readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    w.read(loadDoc.object());

    return a.exec();
}
