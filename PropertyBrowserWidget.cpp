#include <QVariantList>
#include <QJsonArray>

#include "PropertyBrowserWidget.h"
#include "ui_PropertyBrowserWidget.h"

#include"PropertyWidgets/PropertyWidgetInt.h"
#include"PropertyWidgets/PropertyWidgetDouble.h"

PropertyBrowserWidget::PropertyBrowserWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PropertyBrowserWidget)
{
    ui->setupUi(this);


    ui->propertiesTable->setHorizontalHeaderLabels(QList<QString>({"name","value"}));
    ui->propertiesTable->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeMode::Interactive);
}

PropertyBrowserWidget::~PropertyBrowserWidget()
{
    delete ui;
}

void PropertyBrowserWidget::setTitle(const QString &text)
{
    ui->listTitle->setText(text);
}

int PropertyBrowserWidget::addPropertyName(const QString &text)
{
    int rowCount=ui->propertiesTable->rowCount();
    ui->propertiesTable->insertRow(rowCount);
    QLabel* tmpLabel=new QLabel(text);
    tmpLabel->setAlignment(Qt::AlignCenter);
    tmpLabel->setMargin(3);
    ui->propertiesTable->setCellWidget(rowCount,0,tmpLabel);
    return rowCount;
}

void PropertyBrowserWidget::addProperty(Property<int> *property, int min, int max, int step)
{
    /// Вот тут тоже надо переделать

    PropertyWidgetInt *pwidget = new PropertyWidgetInt(property,min,max,step);
    widgets_.append(pwidget);
    ui->propertiesTable->setCellWidget(addPropertyName(property->name()),1,
                                       pwidget);

    connect(pwidget, SIGNAL(valueChanged()), this, SIGNAL(valueChanged()));

    this->resize(minimumSizeHint());
    this->adjustSize();
}

void PropertyBrowserWidget::addProperty(Property<double> *property, double min, double max, double step, int decimals)
{
    PropertyWidgetDouble *pwidget = new PropertyWidgetDouble(property,min,max,step,decimals);
    widgets_.append(pwidget);
    ui->propertiesTable->setCellWidget(addPropertyName(property->name()),1,
                                       pwidget);
    connect(pwidget, SIGNAL(valueChanged()), this, SIGNAL(valueChanged()));
}

bool PropertyBrowserWidget::read(QJsonObject &json)
{
    if(json.isEmpty()) return false;
    auto objList = widgets_;
    auto jsonObj =json[ui->listTitle->text()].toObject();

    for(auto var :objList)
    {
        auto obj=dynamic_cast<PropertyWidgetBase*>(var);
        obj->read(jsonObj);
    }

    emit valueChanged();
    return true;
}

bool PropertyBrowserWidget::write(QJsonObject &json)
{
    QJsonObject jobj;
    auto objList = widgets_;
    for(auto var :objList)
    {
        auto obj=dynamic_cast<PropertyWidgetBase*>(var);
        obj->write(jobj);
    }
    json[ui->listTitle->text()]=jobj;

    return true;
}

void PropertyBrowserWidget::on_rollButton_toggled(bool checked)
{
    if(checked){
        ui->rollButton->setText("+");
        ui->propertiesTable->setVisible(false);
        return;
    }

    ui->rollButton->setText("-");
    ui->propertiesTable->setVisible(true);

}

