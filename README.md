# README #

## РУССКИЙ ##

Это проект редактора свойств.

Требования:

* Qt 5.7 версии или новее. Возможно на более старой тоже будет работать, но я не пробовал.

* Компилятор с поддержкой C++11 (хотя в настройках проекта стоит C++14, но должно работать и на C++11)


Проект связан с другими, но сможет самостоятельно собраться:

* [QtImageViewer](https://bitbucket.org/crazysiberianscientistproj/qtimageviewer)

* [QtCVGUI](https://bitbucket.org/crazysiberianscientistproj/qtcvgui)


### Контакты ###
crazysiberianscientist@gmail.com




## ENGLISH ##

This is project of property editor.

Requerements:

* Qt 5.7 version or newer. Maybe this might works on older version, but I didn't try.

* Compiler with C++11 support (although, in project settings C++14 is setted, this might works on C++11).

This project is linked with others, but can be builded without them:

* [QtImageViewer](https://bitbucket.org/crazysiberianscientistproj/qtimageviewer)

* [QtCVGUI](https://bitbucket.org/crazysiberianscientistproj/qtcvgui)


### Contacts ###
crazysiberianscientist@gmail.com