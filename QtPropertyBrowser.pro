#-------------------------------------------------
#
# Project created by QtCreator 2017-02-12T17:00:15
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtPropertyBrowser
TEMPLATE = app

SOURCES += main.cpp\
        PropertyBrowserWidget.cpp \
    Property.cpp \
    PropertyBase.cpp \
    PropertyWidgets/PropertyWidgetInt.cpp \
    PropertyWidgets/PropertyWidgetBase.cpp \
    PropertyWidgets/PropertyWidgetDouble.cpp

HEADERS  += PropertyBrowserWidget.h \
    Property.h \
    PropertyBase.h \
    PropertyWidgets/PropertyWidgetInt.h \
    PropertyWidgets/PropertyWidgetBase.h \
    PropertyWidgets/PropertyWidgetDouble.h

FORMS    += PropertyBrowserWidget.ui \
    PropertyWidgets/PropertyWidgetInt.ui \
    PropertyWidgets/PropertyWidgetDouble.ui
