#ifndef PROPERTY_H
#define PROPERTY_H

#include <QDebug>

#include <functional>

#include "propertybase.h"

template <typename T>
class Property: public PropertyBase
{
public:
    Property(const QString &name, T (*getFunc)(),void (*setFunc)(T));
    template<typename TO> Property(const QString & name, T (TO::*getFunc)() const, void (TO::*setFunc)(T), TO &object);

    void set(const T &val, bool emitSignal=true);
    T get();

private:
    std::function<T()> getFunc_;
    std::function<void(const T&)> setFunc_;
};

template <typename T>
template <typename TO>
Property<T>::Property(const QString &name, T (TO::*getFunc)() const, void (TO::*setFunc)(T), TO &object)
{
    Q_ASSERT_X(getFunc!=0||setFunc!=0,"ParamItem::init","_getFunc==0||_setFunc==0");
    Q_STATIC_ASSERT_X((std::is_same<T,int>::value||std::is_same<T,double>::value||std::is_same<T,bool>::value),"Unresolved Type in ParamItem::init<Type>");

    name_=name;
    getFunc_=std::bind(getFunc,std::reference_wrapper<TO>(object));
    setFunc_=std::bind(setFunc,std::reference_wrapper<TO>(object),std::placeholders::_1);
}


template <typename T>
Property<T>::Property(const QString &name, T (*getFunc)(), void (*setFunc)(T))
{
    Q_ASSERT_X(getFunc!=0||setFunc!=0,"ParamItem::init","_getFunc==0||_setFunc==0");
    Q_STATIC_ASSERT_X((std::is_same<T,int>::value||std::is_same<T,double>::value||std::is_same<T,bool>::value),"Unresolved Type in ParamItem::init<Type>");

    name_=name;
    getFunc_=getFunc;
    setFunc_=setFunc;

}

template <typename T>
void Property<T>::set(const T &val, bool emitSignal)
{
    qDebug()<<Q_FUNC_INFO;
    qDebug()<<"val:"<<val;
    setFunc_(val);
    if(emitSignal)
        emit valueChanged();
}

template <typename T>
T Property<T>::get()
{
    return getFunc_();
}

#endif // PROPERTY_H
