#TEMPLATE = subdirs

#QT       += core gui

#SUBDIRS = ./ \

CONFIG += no_batch

INCLUDEPATH += \
    $$PWD

SOURCES += \
    $$PWD/PropertyBrowserWidget.cpp \
    $$PWD/Property.cpp \
    $$PWD/PropertyBase.cpp \
    $$PWD/PropertyWidgets/PropertyWidgetInt.cpp \
    $$PWD/PropertyWidgets/PropertyWidgetBase.cpp \
    $$PWD/PropertyWidgets/PropertyWidgetDouble.cpp

HEADERS  += $$PWD/PropertyBrowserWidget.h \
    $$PWD/Property.h \
    $$PWD/PropertyBase.h \
    $$PWD/PropertyWidgets/PropertyWidgetInt.h \
    $$PWD/PropertyWidgets/PropertyWidgetBase.h \
    $$PWD/PropertyWidgets/PropertyWidgetDouble.h

FORMS    += $$PWD/PropertyBrowserWidget.ui \
    $$PWD/PropertyWidgets/PropertyWidgetInt.ui \
    $$PWD/PropertyWidgets/PropertyWidgetDouble.ui
