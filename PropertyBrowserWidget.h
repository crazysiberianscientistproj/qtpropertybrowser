#ifndef PROPERTYBROWSERWIDGET_H
#define PROPERTYBROWSERWIDGET_H

#include <QWidget>
#include <QString>
#include <QWidgetList>
#include <QJsonObject>


#include "Property.h"

namespace Ui {
class PropertyBrowserWidget;
}

class PropertyBrowserWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PropertyBrowserWidget(QWidget *parent = 0);
    ~PropertyBrowserWidget();

    void setTitle(const QString &text);
    void addProperty(Property<int> *property, int min=-10, int max=10, int step=1);
    void addProperty(Property<double> *property, double min=-20, double max=20, double step=1, int decimals=3);

    bool read(QJsonObject &json);
    bool write(QJsonObject &json);

signals:
    void valueChanged();

private slots:
    void on_rollButton_toggled(bool checked);

private:
    int addPropertyName(const QString &text);

    Ui::PropertyBrowserWidget *ui;
    QWidgetList widgets_;

};

#endif // PROPERTYBROWSERWIDGET_H
