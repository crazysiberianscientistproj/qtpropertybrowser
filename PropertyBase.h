#ifndef PROPERTYBASE_H
#define PROPERTYBASE_H

#include <QObject>
#include <QString>

class PropertyBase: public QObject
{
    Q_OBJECT

public:
    QString name() const;
    void setName(const QString &name);

signals:
    void valueChanged();

protected:
    PropertyBase();
    virtual ~PropertyBase(){}

    QString name_;
};

#endif // PROPERTYBASE_H
