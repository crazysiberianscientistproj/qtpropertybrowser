#ifndef PROPERTYWIDGETINT_H
#define PROPERTYWIDGETINT_H

#include <QWidget>

#include "PropertyWidgetBase.h"
#include "../Property.h"

namespace Ui {
class PropertyWidgetInt;
}

class PropertyWidgetInt : public PropertyWidgetBase
{
    Q_OBJECT

public:
    explicit PropertyWidgetInt(Property<int> *property=0, int min=-10, int max=10, int step=1, QWidget *parent = 0);
    ~PropertyWidgetInt();

    bool read(const QJsonObject &file);
    bool write(QJsonObject &file);

    QString name() const;

public slots:
    void onCallPropertyGet();
    void onCallPropertySet();

private:
    Ui::PropertyWidgetInt *ui;

    Property<int> *property_;
};

#endif // PROPERTYWIDGETINT_H
