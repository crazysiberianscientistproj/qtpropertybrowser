#include <QVariantMap>
#include <QJsonObject>

#include "PropertyWidgetDouble.h"
#include "ui_PropertyWidgetDouble.h"

PropertyWidgetDouble::PropertyWidgetDouble(Property<double> *property, double min, double max, double step, int decimals, QWidget *parent) :
    PropertyWidgetBase(parent),
    ui(new Ui::PropertyWidgetDouble)
{
    ui->setupUi(this);

    property_=property;
    property_->setParent(this);
    ui->spinBox->setRange(min,max);
    ui->spinBox->setSingleStep(step);
    ui->spinBox->setDecimals(decimals);

    onCallPropertyGet();

    connect(ui->spinBox,SIGNAL(valueChanged(double)),this,SLOT(onCallPropertySet()));
    connect(property_,SIGNAL(valueChanged()),this,SLOT(onCallPropertyGet()));
}

PropertyWidgetDouble::~PropertyWidgetDouble()
{
    delete ui;
}

bool PropertyWidgetDouble::read(const QJsonObject &file)
{
    if(file.isEmpty()) return false;
    auto json = file[property_->name()];

    property_->set(json.toDouble());
    this->onCallPropertyGet();

    return true;
}

bool PropertyWidgetDouble::write(QJsonObject &file)
{
//    if(file.isEmpty()) return false;

    file[property_->name()]=property_->get();

    return true;
}

QString PropertyWidgetDouble::name() const
{
    return property_->name();
}

void PropertyWidgetDouble::onCallPropertyGet()
{
    this->blockSignals(true);
    ui->spinBox->setValue(property_->get());
    this->blockSignals(false);
}

void PropertyWidgetDouble::onCallPropertySet()
{
    disconnect(property_,SIGNAL(valueChanged()),this,SLOT(onCallPropertyGet()));
    property_->set(ui->spinBox->value());
    connect(property_,SIGNAL(valueChanged()),this,SLOT(onCallPropertyGet()));
    emit valueChanged();
}
