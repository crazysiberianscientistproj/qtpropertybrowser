#ifndef PROPERTYWIDGETDOUBLE_H
#define PROPERTYWIDGETDOUBLE_H

#include <QWidget>

#include "PropertyWidgetBase.h"
#include "../Property.h"

namespace Ui {
class PropertyWidgetDouble;
}

class PropertyWidgetDouble : public PropertyWidgetBase
{
    Q_OBJECT

public:
     PropertyWidgetDouble(Property<double> *property, double min, double max, double step, int decimals, QWidget *parent = 0);
    ~PropertyWidgetDouble();

     bool read(const QJsonObject &file);
     bool write(QJsonObject &file);

     QString name() const;

public slots:
    void onCallPropertyGet();
    void onCallPropertySet();

private:
    Ui::PropertyWidgetDouble *ui;

    Property<double> *property_;
};

#endif // PROPERTYWIDGETDOUBLE_H
