#include <QDebug>

#include "PropertyWidgetInt.h"
#include "ui_PropertyWidgetInt.h"

PropertyWidgetInt::PropertyWidgetInt(Property<int> *property, int min, int max, int step, QWidget *parent):
    PropertyWidgetBase(parent),
    ui(new Ui::PropertyWidgetInt)
{
    ui->setupUi(this);

    property_=property;
    property_->setParent(this);
    ui->spinBox->setRange(min,max);
    ui->spinBox->setSingleStep(step);

    onCallPropertyGet();

    connect(ui->spinBox,SIGNAL(valueChanged(int)),this,SLOT(onCallPropertySet()));
    connect(property_,SIGNAL(valueChanged()),this,SLOT(onCallPropertyGet()));
}

PropertyWidgetInt::~PropertyWidgetInt()
{
    delete ui;
}

bool PropertyWidgetInt::read(const QJsonObject &file)
{
    if(file.isEmpty()) return false;
    auto json = file[property_->name()];

    property_->set(json.toInt());
    this->onCallPropertyGet();

    return true;
}

bool PropertyWidgetInt::write(QJsonObject &file)
{
//    if(file.isEmpty()) return false;

    file[property_->name()]=property_->get();

    return true;
}

QString PropertyWidgetInt::name() const
{
    return property_->name();
}

void PropertyWidgetInt::onCallPropertyGet()
{
    this->blockSignals(true);
    ui->spinBox->setValue(property_->get());
    this->blockSignals(false);
}

void PropertyWidgetInt::onCallPropertySet()
{
    disconnect(property_,SIGNAL(valueChanged()),this,SLOT(onCallPropertyGet()));
    property_->set(ui->spinBox->value());
    connect(property_,SIGNAL(valueChanged()),this,SLOT(onCallPropertyGet()));
    emit valueChanged();
}
