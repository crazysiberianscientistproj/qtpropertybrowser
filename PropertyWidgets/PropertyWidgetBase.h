#ifndef PROPERTYWIDGETBASE_H
#define PROPERTYWIDGETBASE_H

#include <QWidget>
#include <QJsonObject>

class PropertyWidgetBase : public QWidget
{
    Q_OBJECT
public:
    explicit PropertyWidgetBase(QWidget *parent = 0);
    virtual ~PropertyWidgetBase();

    virtual bool read(const QJsonObject &file)=0;
    virtual bool write(QJsonObject &file)=0;

    virtual QString name() const=0;

signals:
    void valueChanged();

public slots:
    /// \todo !!!! Надо переделать механизм наследования этих слотов
    /// Чтобы кучу кода не плодить!!!!!
    virtual void onCallPropertyGet()=0;
    virtual void onCallPropertySet()=0;

};

#endif // PROPERTYWIDGETBASE_H
